const config = require('./config.js');
const core = require('./core.js');

const debug = require('debug')('main');
const bodyParser = require('body-parser');
const cors = require('cors');
const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);

app.use(bodyParser.json());
app.use(cors());

app.use('/api/posts', require('./routes/api/posts'));

if (process.env.NODE_ENV === 'production') {
	debug('Running in Production Mode');
	app.use(express.static(__dirname + '/public/'));
	app.get(/.*/, (req, res) => res.sendFile(__dirname + '/public/index.htm'));
}

io.on('connection', function (socket) {
	debug(`Socket connected ${socket.id}`);

	socket.on('pinger', function (data, callback) {
		debug(data);
		callback(data);
	});
});

const webPort = process.env.PORT || config.webserver.port;
server.listen(webPort, () => {
	debug(`Webserver listening on port ${webPort}`);
});
