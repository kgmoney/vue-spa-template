const path = require('path');

module.exports = {
	outputDir: path.resolve(__dirname, '../server/public'),
	devServer: {
		proxy: {
			'/api': {
				target: 'http://localhost:8000'
			}
		}
	},
	chainWebpack: config => {
		config.module.rule('eslint').use('eslint-loader').options({
			fix: true
		});
	}	
};