const debug = require('debug')('app:main');
localStorage.debug = 'app:*';

import Vue from 'vue';
Vue.config.productionTip = false;

import './assets/fonts/fonts.css';

import Main from './Main.vue';
import router from './router';
import store from './store';

import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';
Vue.use(Vuetify, {
	iconfont: 'md'
});

import VueSocketIO from 'vue-socket.io';
Vue.use(new VueSocketIO({
	debug: false,
	connection: 'http://:8000',
	vuex: {
		store: store,
		actionPrefix: 'SOCKET_',
		mutationPrefix: 'SOCKET_'
	}
}));

new Vue({
	router,
	store,
	render: h => h(Main),
}).$mount('#app');

debug('Main started');