const state = {
	isConnected: false,
	test: 'Test string'
};

const getters = {
};

const actions = {
};

const mutations = {
	SOCKET_connect: (state, value) =>  {
		state.isConnected = true;
	},
	SOCKET_disconnect: (state, value) => {
		state.isConnected = false;
	}
};

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
};
