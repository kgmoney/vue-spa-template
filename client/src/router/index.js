import Vue from 'vue';
import Router from 'vue-router';

import Overview from '../components/Overview.vue';
import Settings from '../components/Settings.vue';

Vue.use(Router);

export default new Router({
	routes: [
		{
			path: '/overview',
			alias: '/',
			component: Overview
		},
		{
			path: '/settings',
			component: Settings
		}
	]
});
